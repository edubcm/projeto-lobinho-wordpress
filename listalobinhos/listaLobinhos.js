const url = 'http://lobinhos.herokuapp.com/wolves/'
const urla = 'http://lobinhos.herokuapp.com/wolves/adopted/'
const fetchConfig = {
  method: 'GET'
}
fetch(url, fetchConfig)
  .then(resposta =>
    resposta
      .json()
      .then(respo => {
        for (i in respo) {
          constructor(
            respo[i].image_url,
            respo[i].name,
            respo[i].age,
            respo[i].description,
            respo[i].id,
            i
          )
        }
      })
      .catch(error => console.log(error))
  )
  .catch(erro => console.log(erro))

const chek = document.querySelector('#filter')

chek.addEventListener('change', function () {
  if (chek.checked) {
    let container = document.querySelector('.container')
    container.innerHTML = ''
    fetch(urla, fetchConfig)
      .then(resposta =>
        resposta
          .json()
          .then(respo => {
            for (i in respo) {
              constructor(
                respo[i].image_url,
                respo[i].name,
                respo[i].age,
                respo[i].description,
                respo[i].id,
                i
              )
              let adopte = document.querySelector(`#adopted${i}`)
              adopte.style.backgroundColor = '#7AAC3A'
              adopte.value = 'Adotado'
              let link = adopte.parentElement
              link.href = ''
            }
          })
          .catch(error => console.log(error))
      )
      .catch(erro => console.log(erro))
  } else {
    let container = document.querySelector('.container')
    container.innerHTML = ''
    fetch(url, fetchConfig)
      .then(resposta =>
        resposta
          .json()
          .then(respo => {
            for (i in respo) {
              constructor(
                respo[i].image_url,
                respo[i].name,
                respo[i].age,
                respo[i].description,
                respo[i].id,
                i
              )
            }
          })
          .catch(error => console.log(error))
      )
      .catch(erro => console.log(erro))
  }
})

function constructor(photo, name, age, description, id, i) {
  if (i % 2 == 0) {
    let sectionwolf = document.createElement('section')
    sectionwolf.className = 'wolf'
    sectionwolf.id = 'wolf'
    sectionwolf.innerHTML = `<div class='moldura'><div class='frame'></div><img src='../assets/image.png' alt='' srcset='' width='380' height='320' class='invertedImage' id='img${i}'></div><div class='text'><div class='divnome-lobo' ><h1 class='nome-lobo' id='nome${i}'>Nome do Lobo</h1><a href="../showLobinho/showLobinho.html"><input type='button' value='Adotar' id='adopted${i}'></a></div><div class='idade-lobo' id='idade${i}'>XX</div><div class='descricao' id='description${i}'>Lorem ipsum dolor</div></div></div>`
    document.body.querySelector('.container').appendChild(sectionwolf)

    let image = document.querySelector(`#img${i}`)
    let nome = document.querySelector(`#nome${i}`)
    let idade = document.querySelector(`#idade${i}`)
    let descricao = document.querySelector(`#description${i}`)

    image.src = photo
    nome.innerText = name
    idade.innerText = `Idade: ${age} anos`
    descricao.innerText = description

    let btnAdopted = document.querySelector(`#adopted${i}`)
    btnAdopted.addEventListener('click', function () {
      let text = JSON.stringify(id)
      sessionStorage.setItem('idlobo', text)
    })
  } else {
    let sectionwolfinverted = document.createElement('section')
    sectionwolfinverted.className = 'wolfinverted'
    sectionwolfinverted.id = 'wolfinverted'
    sectionwolfinverted.innerHTML = `<div class='invertedMoldura'><div class='frame'></div><img src='../assets/image.png' alt='' srcset='' width='380' height='320' class='invertedImage' id='img${i}inverted'></div><div class='text-inverted'><div class='divnome-invertido' ><h1 class='nome-inverted' id='nome${i}inverted'>Nome do Lobo</h1><a href="../showLobinho/showLobinho.html"><input type='button' value='Adotar' id='adopted${i}'></a></div><div class='idade-invertido' id='idade${i}inverted'>XX</div><div class='descricao-invertido' id='description${i}inverted'>Lorem ipsum dolor</div></div></div>`
    document.body.querySelector('.container').appendChild(sectionwolfinverted)

    let image = document.querySelector(`#img${i}inverted`)
    let nome = document.querySelector(`#nome${i}inverted`)
    let idade = document.querySelector(`#idade${i}inverted`)
    let descricao = document.querySelector(`#description${i}inverted`)

    image.src = photo
    nome.innerText = name
    idade.innerText = `Idade: ${age} anos`
    descricao.innerText = description

    let btnAdopted = document.querySelector(`#adopted${i}`)
    btnAdopted.addEventListener('click', function () {
      let text = JSON.stringify(id)
      sessionStorage.setItem('idlobo', text)
    })
  }
}

function search() {
  let input = document.querySelector('#search')
  let filter = input.value.toUpperCase()
  let menu = document.querySelector('.container')
  let itensMenu = menu.getElementsByTagName('section')

  for (i = 0; i < itensMenu.length; i++) {
    names = itensMenu[i].getElementsByTagName('h1')[0]
    if (names.innerHTML.toUpperCase().indexOf(filter) > -1) {
      itensMenu[i].style.display = ''
    } else {
      itensMenu[i].style.display = 'none'
    }
  }
}
