<?php get_header(); ?>
    <main>
      <section class="loboInicial">
        <div class="textos">
          <h1><?php the_field('titulo_inicial'); ?></h1>
          <pre class="underline"> ___</pre>
          <p><?php the_field('descricao_inicial'); ?></p>
        </div>
      </section>
      <section class="about">
        <div class="divAbout">
          <h2>Sobre</h2>
          <p>
            Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
          </p>
        </div>
      </section>
      <section class="valores">
        <h2>Valores</h2>
        <div class="cards">
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone1.png" alt="" srcset="" />
            <h3>Proteção</h3>
            <p class="pCard">
              Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone2.png" alt="" srcset="" />
            <h3>Carinho</h3>
            <p class="pCard">
              Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone3.png" alt="" srcset="" width="144" height="145"/>
            <h3>Companheirismo</h3>
            <p class="pCard">
              Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone4.png" alt="" srcset="" />
            <h3>Resgate</h3>
            <p class="pCard">
              Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
            </p>
          </div>
        </div>
      </section>
      <section class="loboExemple">
        <div class="titulo-exemplo">
          <h2>Lobos Exemplo</h2>
        </div>
        
        <div class="container">
          <div class="wolf">
            <div class="moldura">

              <div class="frame"></div>
              <img src="" alt="" srcset="" width="300" height="300"id="imagem">     
            </div>
            <div class="text">
              <div class="nome-lobo" ><h1 id="nome"></h1></div>
              <div class="idade-lobo" id="idade"></div>
              <div class="descricao" id="description"></div>
            </div>
          </div>
          <div class="wolf" id="inverted">
            <div class="moldura" id="invertedMoldura">

              <div class="frame"></div>
              <img src="" alt="" srcset="" width="300" height="300" id="invertedImage">     
            </div>
            <div class="text" id="text-inverted">
              <div class="nome-lobo"><h1 id="nome-invertido"></h1></div>
              <div class="idade-lobo" id="idade-invertido"></div>
              <div class="descricao" id="descricao-invertido"></div>
            </div>
        </div>
        
      </section>
    </main>
    <div class="divider"></div>
<?php get_footer(); ?>
