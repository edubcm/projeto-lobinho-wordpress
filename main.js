const url = 'http://lobinhos.herokuapp.com/wolves/'

let fetchConfig = {
  method: 'GET'
}

fetch(url, fetchConfig)
  .then(resposta =>
    resposta
      .json()
      .then(respo => {
        constructor(
          respo[getRandomInt(0, respo.length)].image_url,
          respo[getRandomInt(0, respo.length)].name,
          respo[getRandomInt(0, respo.length)].age,
          respo[getRandomInt(0, respo.length)].description
        )
        constructorInverted(
          respo[getRandomInt(0, respo.length)].image_url,
          respo[getRandomInt(0, respo.length)].name,
          respo[getRandomInt(0, respo.length)].age,
          respo[getRandomInt(0, respo.length)].description
        )
      })
      .catch(error => console.log(error))
  )
  .catch(erro => console.log(erro))

function constructor(photo, name, age, description) {
  let image = document.querySelector('#imagem')
  let nome = document.querySelector('#nome')
  let idade = document.querySelector('#idade')
  let descricao = document.querySelector('#description')

  image.src = photo
  nome.innerText = name
  idade.innerText = `Idade: ${age} anos`
  descricao.innerText = description
}

function constructorInverted(photo, name, age, description) {
  let image = document.querySelector('#invertedImage')
  let nome = document.querySelector('#nome-invertido')
  let idade = document.querySelector('#idade-invertido')
  let descricao = document.querySelector('#descricao-invertido')

  image.src = photo
  nome.innerText = name
  idade.innerText = `Idade: ${age} anos`
  descricao.innerText = description
}

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}
