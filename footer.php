<footer>
      <div class="informacoes">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.7537673028!2d-43.1332584!3d-22.9064193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1659300196793!5m2!1spt-BR!2sbr" width="250" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        <div class="contato">
          <div>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/Vector.png"></img>
            <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</p>
          </div>
          <pre><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/telefone.png"></img>     (99) 99999-9999</pre>
          <pre><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/email.png"></img>     salve-lobos@lobinhos.com</pre>
          <a href="quemSomos/quemSomos.html">
            <input type="button" value="Quem Somos">
          </a>
        </div>
        
      </div>
      
      

      <div class="logo">
        <p>Desenvolvido com</p>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/paws.png"></img>
      </div>
    </footer>
    <?php wp_footer(); ?>
    <script src="main.js"></script>
  </body>
</html>